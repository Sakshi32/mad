
import UIKit


class ErrorEntity: NSObject {
    var errorMessage = ""
    convenience init(withMessage: String) {
        self.init()
        self.errorMessage = withMessage
    }
}

class ResponseEntity: NSObject {
    var message = "Some Error"
    var success = true
    convenience init(withMessage: String) {
        self.init()
        
        self.message = withMessage
        
        
    }
}
class Common: NSObject
{
    
    static let Title = "M.A.D"
    // MARK: - Email Validation
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    class func isValidateNumber(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    class func isCheckPasswordLength(password: String)  -> Bool{
        if password.count <= 7{
            return true
        }else{
            return false
        }
    }
    
    class func isCheckOTPLength(password: String)  -> Bool{
        if password.count == 4{
            return true
        }else{
            return false
        }
    }
    
    class func isCheckPasswordLength(password: String , confirmPassword : String) -> Bool {
        if password.count <= 7 && confirmPassword.count <= 7{
            return true
        }else{
            return false
        }
    }
    
    class func ShowAlert( Title : String, Message : String, VC: UIViewController ){
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        //alert.view.tintColor = UIColor.init(red: 0.0/255.0, green: 128.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        //alert.view.layer.cornerRadius = 12
        //alert.view.layer.masksToBounds = true
        //alert.view.layer.borderWidth = 3
        //alert.view.layer.borderColor = UIColor.init(red: 0.0/255.0, green: 128.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        alert.addAction(okAction)
        alert.self.show(VC, sender: self)
        VC.present(alert, animated: true, completion: nil)
    }

    // MARK: - get Day date Month From Date String
    class func getDateDetailFrom(strDate:String) -> String{
        if strDate.isEmpty{
            return ""
        }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "YYYY-MM-dd'T'HH:mm:ss'Z'"
        var date = Date()
        date = dateFormatterGet.date(from: strDate)!
        dateFormatterGet.dateFormat = "EEEE MMM dd yyyy hh-a"
        let strReturn:String = dateFormatterGet.string(from: date)
        return strReturn
    }
    // MARK: - Convert  String To date
    
    class func getDateFromString(strDate:String) -> Date
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSZ"
       // dateFormatterGet.timeZone = TimeZone(identifier: )
        var date = Date()
        date = dateFormatterGet.date(from: strDate)!
         return date
    }
    
  
    // MARK: - Creates a UIColor from a Hex string.
    
    static func hexStringToUIColor (_ hex:String) -> UIColor
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    // MARK: - showHUD with message
    class func showHUD()
    {
       // HUDManager.sharedInstance.showHud()
    }
    // MARK: - hide HUD
    class func hideHUD()
    {
      //  HUDManager.sharedInstance.hideHud()
    }
//    class func sizeForText(text: String, font: UIFont, maxSize: CGSize) -> CGSize
//    {
//        let attrString = NSAttributedString.init(string: text, attributes: [NSAttributedStringKey.font:font])
//        let rect = attrString.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
//        let size = CGSize(width: rect.size.width, height: rect.size.height)
//        return size
//    }
    
    ///// Push
    class func PushMethod(VC: UIViewController, identifier : String){
        let Vc = VC.storyboard?.instantiateViewController(withIdentifier: identifier)
        VC.navigationController?.pushViewController(Vc!, animated: false)
    }
    
    ////Pop
    class func PopMethod(VC: UIViewController){
        _ = VC.navigationController?.popViewController(animated: true)
    }
    
    ////Pop
    class func PopRootMethod(VC: UIViewController){
        _ = VC.navigationController?.popToRootViewController(animated: true)
    }
    
    //// Present
    class func PresentMethod(VC: UIViewController, PresentVC : UIViewController){
        VC.present(    PresentVC , animated: true, completion: nil)
    }
    
    //// Dismiss
    class func DismissMethod(VC: UIViewController){
        VC.dismiss(animated: true, completion: nil)
    }
    
    class func CheckArray(array : Array<Any>) -> Bool {
        var Available : Bool
        
        if (array.count == 0)  {
            Available = false
        }else if (array.isEmpty){
            Available = false
        }
        else{
            Available = true
        }
        return Available
    }
    
    class func CheckMutableArray(array : NSMutableArray) -> Bool {
        var Available : Bool
        
        if (array.count == 0)  {
            Available = false
        }else{
            Available = true
        }
        return Available
    }
    
    
    class func CheckString(checkString : String?) -> Bool {
        
        if (checkString == nil) {
            return false
        }
        else if checkString?.characters.count == 0 {
            return false
        }
        else{
            return true
        }
    }
    
    class func getCurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let result = formatter.string(from: date)
        return result
    }
    
    class func getCurrentTime() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm a"
        let result = formatter.string(from: date)
        return result
    }
    
    class func changeTimeFormat(strDate : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date = dateFormatter.date(from: strDate)
        
        dateFormatter.dateFormat = "HH:mm a"
        let date24 = dateFormatter.string(from: date!)
        
        return date24
    }
    
    class func changeDateFormat(strDate : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: strDate)
        
        
        dateFormatter.dateFormat = "dd MMM YY"
        let date24 = dateFormatter.string(from: date!)
        
        return date24
    }
    
    class func ChangeDateFormat(Date: String, fromFormat: String, toFormat: String ) -> String  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        let newdate = dateFormatter.date(from: Date)
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: newdate!)
    }
    
    ////////////////  Camera Gallery start ///////////////////////
    class func ActionSheetForGallaryAndCamera(Picker : UIImagePickerController, VC: UIViewController) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            /////////// gallery //////////
            self.openGallary(Picker: Picker, VC: VC)
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            /////////// camera //////////
            self.openCamera(Picker: Picker, VC: VC)
        }
        actionSheetController.addAction(deleteActionButton)
        
        VC.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    class func openGallary(Picker : UIImagePickerController, VC: UIViewController)
    {
        Picker.allowsEditing = false
        Picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        VC.present(Picker, animated: true, completion: nil)
    }
    
    class func openCamera(Picker : UIImagePickerController, VC: UIViewController)
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            Picker.allowsEditing = false
            Picker.sourceType = UIImagePickerControllerSourceType.camera
            Picker.cameraCaptureMode = .photo
            VC.present(Picker, animated: true, completion: nil)
        }else{
           // self.ShowAlert(Title: "Camera Not Found", Message: "This device has no Camera", VC: VC)
        }
    }
    ///////////////////// End ///////////////////////
    
}

extension UIViewController {
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}




protocol MyPickerViewProtocol {
    func myPickerDidSelectRow(selectedRowValue:Int?)
}

class MyPickerView: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    
     var MyPickerDelegate:MyPickerViewProtocol?
     var  arrItems  = [String]()
    
    convenience init(withDictionary response:[String]) {
        self.init()
        self.arrItems = response
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrItems.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrItems[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.MyPickerDelegate?.myPickerDidSelectRow(selectedRowValue: row)
    }
}



