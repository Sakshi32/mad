//
//  URLFile.swift
//  RoomOnShare
//
//  Created by mac on 28/07/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import Foundation

class Url_File {
    //static let BaseUrl = "http://mobileapplicationdevelopment.uk/Mad_App/"
    static let BaseUrl = "http://mobileapplicationdevelopment.uk/Mad_App_dev/"
    
    static let Login = Url_File.BaseUrl + "login.php"
    static let ImageSlider = Url_File.BaseUrl + "image_slider.php"
    static let Offer = Url_File.BaseUrl + "offerimage.php?ACTION=GET"
    static let Gallery = Url_File.BaseUrl + "galleryimage.php?ACTION=GET"
    static let About = Url_File.BaseUrl + "about_me.php?ACTION=GET"
    static let ContactUs = Url_File.BaseUrl + "contact_us.php"
    static let AboutEdit = Url_File.BaseUrl + "about_me.php"
    static let AddGalleryImage = Url_File.BaseUrl + "galleryimage.php?ACTION=ADD"
    static let DeleteGalleryImage = Url_File.BaseUrl + "galleryimage.php?ACTION=DELETE"
    static let AddOfferImage = Url_File.BaseUrl + "offerimage.php?ACTION=ADD"
    static let DeleteOfferImage = Url_File.BaseUrl + "offerimage.php?ACTION=DELETE"
    static let Slider_Add = Url_File.BaseUrl + "slider.php"
    static let AllBooking = Url_File.BaseUrl + "all_booking.php"
    static let AddBooking = Url_File.BaseUrl + "add_slot.php"
    static let UpdateBooking = Url_File.BaseUrl + "update_booking.php"
    static let CancelBooking = Url_File.BaseUrl + "cancel_booking.php"
    static let AcceptBooking = Url_File.BaseUrl + "accept_booking.php"
    static let DeleteBooking = Url_File.BaseUrl + "delete_booking.php"    
    static let RequestBooking = Url_File.BaseUrl + "request_booking.php"
    static let MyAvailability = Url_File.BaseUrl + "slot.php"
    
    
    

}
