//
//  HomeCollectionViewCell.swift
//  MAD
//
//  Created by mac on 12/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var imgitem: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
