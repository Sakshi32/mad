//
//  GalleryTableViewCell.swift
//  MAD
//
//  Created by mac on 12/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class GalleryTableViewCell: UITableViewCell {

    @IBOutlet weak var imgItem: UIImageView!    
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if SharedPreference.getIsUserLogin(){
            if SharedPreference.getUserType() == "Admin"{
                self.btnDelete.isHidden = false
            }else{
                self.btnDelete.isHidden = true
            }
        }else{
            self.btnDelete.isHidden = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
