//
//  GalleryViewController.swift
//  MAD
//
//  Created by mac on 12/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController {
    
    @IBOutlet weak var tblGallery: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    
    var Imagepicker = UIImagePickerController()
    var Pickimage : UIImage? = nil
    var arrGallery : Array<Any> = []
    var image_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Imagepicker.delegate = self
        self.tblGallery.register(UINib(nibName: "GalleryTableViewCell", bundle: nil), forCellReuseIdentifier: "Gallery")
        self.GalleryService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sideMenuController?.isLeftViewDisabled = true
        if SharedPreference.getIsUserLogin(){
            if SharedPreference.getUserType() == "Admin"{
                self.btnAdd.isHidden = false
            }else{
                self.btnAdd.isHidden = true
            }
        }else{
            self.btnAdd.isHidden = true
        }
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func AddImageMethod(_ sender: Any) {
        Common.ActionSheetForGallaryAndCamera(Picker: Imagepicker, VC: self)
    }
    
    //////Gallery//////
    func GalleryService() {
        
        self.startActivityIndicator()
        
        CommunicationManager().getResponseFor(strUrl: Url_File.Gallery, parameters: nil, completion: { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.arrGallery = dataDict["galleryimages"] as! Array<Any>
                //self.arrGallery = dic.value(forKey: "galleryimages") as! Array<Any>
                self.tblGallery.reloadData()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        })
    }
    
    //////////// Delete Gallery item  ////////
    func DeleteService(){
        self.startActivityIndicator()
        let param = ["gallery_image_id": self.image_id] as [String : Any]
        
        CommunicationManager().getResponseForParamType(strUrl: Url_File.DeleteGalleryImage, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.stopActivityIndicator()
                self.GalleryService()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    //////////// Add Gallery item  ////////
    func AddImageService(){
        
        self.startActivityIndicator()
        let param = [:] as [String : Any]
        let img = self.Pickimage
        let imgData = UIImageJPEGRepresentation(img!, 0.5)!
        
        CommunicationManager().getResponseForMultipartType(strUrl: Url_File.AddGalleryImage, parameters: param as NSDictionary, imagesData: [imgData], imageKey: "image", check: "", arrphotoid: []) { (result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.stopActivityIndicator()
                self.GalleryService()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}

extension GalleryViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrGallery.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tblGallery.dequeueReusableCell(withIdentifier: "Gallery") as! GalleryTableViewCell
        
        let dic  = self.arrGallery[indexPath.row] as! NSDictionary
        let img = dic.value(forKey: "image") as! String
        cell.imgItem.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "placeholder"))
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(DeleteMethod(btn:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    ////////////// delete button ///////////
    @objc func DeleteMethod(btn : UIButton) {
    
        let Index = btn.tag
        let dic = self.arrGallery[Index] as! NSDictionary
        self.image_id =  dic.value(forKey: "gallery_image_id") as! String
        self.DeleteService()
//        let alert = UIAlertController(title: Common.Title, message:
//            "Are you sure, you want to delete this item", preferredStyle: UIAlertControllerStyle.alert)
//        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) in
//
//        }
//
//        let CancleAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (action) in
//
//        }
//
//        alert.addAction(okAction)
//        alert.addAction(CancleAction)
//        self.present(alert, animated: true, completion: nil)
    }
}

extension GalleryViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage]
        self.Pickimage = image as? UIImage
        picker.dismiss(animated: true, completion: nil)
        self.OpenEditor()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func OpenEditor(){
        guard let image = self.Pickimage else {
            return
        }
        // Use view controller
        let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: false, completion: nil)
    }
    
    // MARK: - CropView
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true, completion: nil)
        self.Pickimage = image
        self.AddImageService()
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

