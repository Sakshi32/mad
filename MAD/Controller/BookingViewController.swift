//
//  BookingViewController.swift
//  MAD
//
//  Created by mac on 13/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class BookingViewController: UIViewController {

    @IBOutlet weak var tblBooking: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    
    var arrBookingList : Array<Any> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblBooking.register(UINib(nibName: "BookingTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sideMenuController?.isLeftViewDisabled = true
        if SharedPreference.getIsUserLogin(){
            if SharedPreference.getUserType() == "Admin"{
                self.btnAdd.isHidden = false
                self.AllBooking()
            }else{
                self.btnAdd.isHidden = true
                self.UserAllBooking()
            }
        }else{
            self.btnAdd.isHidden = true
        }
    
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func AddBookingMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "AddBookingViewController")
    }
    //////// All Booking Admin ////////
    func AllBooking(){
        self.startActivityIndicator()
        let param = ["ACTION"    : "GET",
                     "user_type" : "admin"] as [String : Any]
        
        CommunicationManager().getResponseForParamType(strUrl: Url_File.AllBooking, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let arrres = dataDict["response"] as! Array<Any>
                let dic = arrres[0] as! NSDictionary
                self.arrBookingList  = dic.value(forKey: "all_booking") as! Array<Any>
                self.tblBooking.reloadData()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    
    //////// All Booking User ////////
    func UserAllBooking(){
        self.startActivityIndicator()
        let param = ["ACTION"    : "GET",
                     "user_type" : "user",
                     "user_id" : SharedPreference.getUserId()] as [String : Any]
        
        CommunicationManager().getResponseForParamType(strUrl: Url_File.AllBooking, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let arrres = dataDict["response"] as! Array<Any>
                let dic = arrres[0] as! NSDictionary
                self.arrBookingList  = dic.value(forKey: "all_booking") as! Array<Any>
                self.tblBooking.reloadData()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    //////// Request Booking ////////
    func RequestBooking(dic : NSDictionary){
        self.startActivityIndicator()
        let param = ["ACTION"    : "ADD",
                     "user_id" : dic.value(forKey: "client_id") as! String,
                     "booking_id" : dic.value(forKey: "booking_id") as! String] as [String : Any]
    
        CommunicationManager().getResponseForParamType(strUrl: Url_File.RequestBooking, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.AllBooking()
               
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    ///////// Cancel /////////
    func CancelBooking(dic : NSDictionary){
        self.startActivityIndicator()
        let param = ["user_id"    : dic.value(forKey: "client_id") as! String,
                     "user_type" : "user",
                     "booking_id" : dic.value(forKey: "booking_id") as! String] as [String : Any]
        
        CommunicationManager().getResponseForParamType(strUrl: Url_File.CancelBooking, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}

extension BookingViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBookingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tblBooking.dequeueReusableCell(withIdentifier: "Cell") as! BookingTableViewCell
        
        let dic = self.arrBookingList[indexPath.row] as! NSDictionary
        
        cell.lblTime.text = Common.changeTimeFormat(strDate: dic.value(forKey: "booking_time") as! String )
        cell.lblDate.text = Common.changeDateFormat(strDate: dic.value(forKey: "booking_date") as! String ) //"10 Sep 18"
        let status = dic.value(forKey: "status") as? String
        
        switch status {
        case "1":
            cell.lblStatus.textColor = self.UIColorFromHex(rgbValue: 0xfb27b627, alpha: 1.0)
            cell.lblStatus.text = "AVAILABLE"
        case "2":
            cell.lblStatus.textColor = self.UIColorFromHex(rgbValue: 0xFF8C00, alpha: 1.0)
            cell.lblStatus.text = "REQUESTED"
        case "3":
            cell.lblStatus.textColor = self.UIColorFromHex(rgbValue: 0xcc1b1e, alpha: 1.0)
            cell.lblStatus.text = "CANCELED"
        case "4":
            cell.lblStatus.textColor = self.UIColorFromHex(rgbValue: 0xFF1976D2, alpha: 1.0)
            cell.lblStatus.text = "BOOKED"
        default:
            cell.lblStatus.text = ""
        }
    
        cell.lblName.text = dic.value(forKey: "client_name") as? String
        cell.lblHours.text = String("\(dic.value(forKey: "duration") as! String) hour")

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic = self.arrBookingList[indexPath.row] as! NSDictionary
       // let status = dic.value(forKey: "status") as! String
         // let user = userDef.value(forKey: "user") as! String
        if let status = dic.value(forKey: "status") as? String{
            
            if SharedPreference.getIsUserLogin(){
                if SharedPreference.getUserType() == "Admin"{
                    let activity = storyboard?.instantiateViewController(withIdentifier: "BookingDetailViewController") as! BookingDetailViewController
                    activity.dicbooking = self.arrBookingList[indexPath.row] as! NSDictionary
                    self.navigationController?.pushViewController(activity, animated: true)
                }else{
                    switch status {
                    case "1":
                        let alert = UIAlertController(title: "Request Booking!", message:
                            "Request admin to confirm this booking for you.", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) in
                            self.RequestBooking(dic: dic)
                        }
                        let CancleAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (action) in
                            
                        }
                        alert.addAction(okAction)
                        alert.addAction(CancleAction)
                        self.present(alert, animated: true, completion: nil)
                    case "2":
                        let alert = UIAlertController(title: "Cancel Booking!", message:
                            "Are you sure you want to cancel this booking.", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) in
                            self.CancelBooking(dic: dic)
                        }
                        let CancleAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (action) in
                            
                        }
                        alert.addAction(okAction)
                        alert.addAction(CancleAction)
                        self.present(alert, animated: true, completion: nil)
                    default: break
                    }
                    
                }
            }
            
        }

    }
    
}
