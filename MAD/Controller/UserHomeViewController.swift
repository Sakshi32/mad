//
//  UserHomeViewController.swift
//  MAD
//
//  Created by mac on 16/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class UserHomeViewController: UIViewController , UIScrollViewDelegate{

    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    var offSet: CGFloat = 0
    
    @IBOutlet weak var tblUserHome: UITableView!
    
    var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
    
     var arrImages : [String] = []
    var arrOptions = ["GUEST",  "BOOKINGS", "WORKOUTS", "MEAL PLANS", "CHANGE BANNER", "ABOUT ME", "MY AVAILABILITY", "LOGOUT"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.ImageSliderService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sideMenuController?.isLeftViewDisabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
    }
    
    @objc func autoScroll (){
        let totalPossibleOffset = CGFloat(self.arrImages.count - 1) * self.view.bounds.size.width
        if offSet == totalPossibleOffset {
            offSet = 0 // come back to the first image after the last image
        }
        else {
            offSet += self.view.bounds.size.width
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.scrollview.contentOffset.x = CGFloat(self.offSet)
                //self.pageControl.currentPage = self.arrImages.count
            }, completion: nil)
        }
    }
    
    func SetupPages(){
        let screenSize = UIScreen.main.bounds
        let width = screenSize.width
        let hight = screenSize.height - 157
        for index in 0..<self.arrImages.count{
            let strImg = self.arrImages[index]
            let imageView = UIImageView()
            let xPosition = UIScreen.main.bounds.width * CGFloat(index)
            imageView.frame = CGRect(x: xPosition, y: 0, width: scrollview.frame.width, height: 175)
            imageView.sd_setImage(with: URL(string: strImg), placeholderImage: UIImage(named: ""))
            imageView.contentMode = .scaleToFill
            
            self.scrollview.contentSize = CGSize(width: width * CGFloat(index + 1),height: hight -  hight)
            self.scrollview.addSubview(imageView)
        }
        
        pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
    }
    
    func configurePageControl() {
        self.pageControl.numberOfPages = self.arrImages.count
        self.pageControl.currentPage = 0
        self.pageControl.currentPageIndicatorTintColor = UIColor.blue
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    @objc func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * scrollview.frame.size.width
        scrollview.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    

  
    
    func ImageSliderService(){
        self.startActivityIndicator()
        let param = ["SLIDER": "TRUE"] as [String : Any]
        
        CommunicationManager().getResponseForParamType(strUrl: Url_File.ImageSlider, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                self.stopActivityIndicator()
                
                let dataDict = data as! NSDictionary
                print(dataDict)
                
                let arrImages = dataDict["image"] as! [NSDictionary]
                for i in 0..<arrImages.count{
                    let dict = arrImages[i]
                    let strImg = dict["img"] as! String
                    self.arrImages.append(strImg)
                }
                self.SetupPages()
                self.configurePageControl()
                
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}


extension UserHomeViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tblUserHome.dequeueReusableCell(withIdentifier: "Cell") as! UserHomecell
        
        cell.btnOptios.setTitle(self.arrOptions[indexPath.row] as String, for: .normal)
        cell.btnOptios.tag = indexPath.row
        cell.btnOptios.addTarget(self, action: #selector(DeleteMethod(btn:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Common.PushMethod(VC: self, identifier: "BookingDetailViewController")
    }
    
    @objc func DeleteMethod(btn : UIButton) {
        
        let Index = btn.tag
        switch Index {
        case 0:
           // Common.PushMethod(VC: self, identifier: "")
            break
        case 1:
            Common.PushMethod(VC: self, identifier: "BookingViewController")
            break
        case 2:
            Common.PushMethod(VC: self, identifier: "OfferViewController")
        case 3:
            Common.PushMethod(VC: self, identifier: "GalleryViewController")
            break
        case 4:
           //Common.PushMethod(VC: self, identifier: "")
            break
        case 5:
            //Common.PushMethod(VC: self, identifier: "")
            break
        case 6:
            Common.PushMethod(VC: self, identifier: "MyAvailabilityViewController")
            break
        case 7:
            SharedPreference.setIsUserLogin(false)
            SharedPreference.setUserType("")
            NotificationCenter.default.post(name: Notification.Name("Reload"), object: nil)
            let mainViewController = sideMenuController!
            let navigationController = mainViewController.rootViewController as! NavigationController
            let viewController: UIViewController!
            viewController = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController")
            navigationController.setViewControllers([viewController], animated: false)
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
        default: break
            //Common.PushMethod(VC: self, identifier: "")
        }
        
    }
}

class UserHomecell: UITableViewCell {
    @IBOutlet weak var btnOptios: UIButton!
}
