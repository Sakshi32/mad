//
//  ChangeBanneViewController.swift
//  MAD
//
//  Created by mac on 13/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class ChangeBanneViewController: UIViewController {

    
    var Imagepicker = UIImagePickerController()
    var Pickimage : UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.isLeftViewDisabled = true
        Imagepicker.delegate = self       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }

    @IBAction func PickMethod(_ sender: Any) {
         Common.ActionSheetForGallaryAndCamera(Picker: Imagepicker, VC: self)
    }
    
    @IBAction func AddMethod(_ sender: Any) {
        if self.Pickimage != nil{
            self.AddImageService()
        }else{
            Common.ShowAlert(Title: Common.Title, Message: "Please select image", VC: self)
        }
    }
    
    @IBAction func ViewAllMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "AllViewViewController")
    }
    
    //////////// Add Banner Image  ////////
    func AddImageService(){
        
        self.startActivityIndicator()
        
        let img = self.Pickimage
        let imgData = UIImageJPEGRepresentation(img!, 0.5)!
        
        let param = ["ACTION": "ADD",
                     "TYPE" : "SLIDER"] as [String : Any]
       
        CommunicationManager().getResponseForMultipartType(strUrl: Url_File.Slider_Add, parameters: param as NSDictionary, imagesData: [imgData], imageKey: "img", check: "", arrphotoid: []) { (result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.stopActivityIndicator()
                Common.ShowAlert(Title: Common.Title, Message: "Image add successfully", VC: self)
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
}

extension ChangeBanneViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage]
        self.Pickimage = image as? UIImage
        picker.dismiss(animated: true, completion: nil)
        self.OpenEditor()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func OpenEditor(){
        guard let image = self.Pickimage else {
            return
        }
        // Use view controller
        let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: false, completion: nil)
    }
    
    // MARK: - CropView
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true, completion: nil)
        self.Pickimage = image
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
