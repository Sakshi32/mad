//
//  LoginViewController.swift
//  MAD
//
//  Created by mac on 13/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func ActionBack(_ sender: Any)
    {
       
        Common.PopMethod(VC: self)
        
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! NavigationController
        let viewController: UIViewController!
        viewController = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController")
        navigationController.setViewControllers([viewController], animated: false)
        mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sideMenuController?.hideLeftView()
        sideMenuController?.isLeftViewDisabled = true
    }

    @IBAction func LoginMethod(_ sender: Any) {
        guard let name = self.txtUsername.text, name != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter Name", VC: self)
        }
        
        guard let contact = self.txtPassword.text, contact != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter contact number", VC: self)
        }
        
        self.LoginService()
    }
    
    ///////////// Login ////////////
    func LoginService(){
        self.startActivityIndicator()
        let param = ["LOGIN":"TRUE",
                     "username": self.txtUsername.text!,
                     "pass" : self.txtPassword.text!] as [String : Any]
    
        CommunicationManager().getResponseForParamType(strUrl: Url_File.Login, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let arrres = dataDict["responce"] as! Array<NSDictionary>
                self.Next(dic: arrres[0])
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func Next(dic : NSDictionary) {
        
       
        
        if dic.value(forKey: "user") as! String == "ADMIN"{
            SharedPreference.setIsUserLogin(true)
            SharedPreference.setUserType("Admin")
            NotificationCenter.default.post(name: Notification.Name("Reload"), object: nil)
            let activity = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(activity, animated: true)
        }else{
           // SharedPreference.setUserType("User")
           // SharedPreference.setUserId(dic.value(forKey: "id") as! String)
               Common.ShowAlert(Title: Common.Title, Message: "Invalid User", VC: self)
            
            //let activity = storyboard?.instantiateViewController(withIdentifier: "UserHomeViewController") as! UserHomeViewController
           // self.navigationController?.pushViewController(activity, animated: true)
        }
    }

    
}
