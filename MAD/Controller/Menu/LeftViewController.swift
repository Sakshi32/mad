//
//  LeftViewController.swift
//  EventFully
//
//  Created by mac on 22/08/17.
//  Copyright © 2017 espsofttech. All rights reserved.
//

import UIKit

class LeftViewController: UIViewController {
    
    @IBOutlet weak var tblLeftSide: UITableView!
    
    var titlesArray1 : Array<Any> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if SharedPreference.getIsUserLogin(){
            self.titlesArray1 = ["Logout",
                                 "About us"]
        }else{
            self.titlesArray1 = ["Login",
                                 "About us",
                                 "Contact us"]
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("Reload"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func methodOfReceivedNotification(notification: Notification){
        if SharedPreference.getIsUserLogin(){
            self.titlesArray1 = ["Logout",
                                 "About us"]
        }else{
            self.titlesArray1 = ["Login",
                                 "About us",
                                 "Contact us"]
        }
        self.tblLeftSide.reloadData()
    }
    
}

extension LeftViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return titlesArray1.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! LeftViewCell
        cell.lblname.text = self.titlesArray1[indexPath.row] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! NavigationController
        let viewController: UIViewController!
        if SharedPreference.getIsUserLogin(){
            if indexPath.row == 0  {
                SharedPreference.setIsUserLogin(false)
                SharedPreference.setUserType("")
                NotificationCenter.default.post(name: Notification.Name("Reload"), object: nil)
                viewController = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController")
                navigationController.setViewControllers([viewController], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if indexPath.row == 1  {
                viewController = self.storyboard!.instantiateViewController(withIdentifier: "AboutUsViewController")
                navigationController.setViewControllers([viewController], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
        }else{
            if indexPath.row == 0  {
                viewController = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController")
                navigationController.setViewControllers([viewController], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if indexPath.row == 1  {
                viewController = self.storyboard!.instantiateViewController(withIdentifier: "AboutUsViewController")
                navigationController.setViewControllers([viewController], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if indexPath.row == 2  {
                viewController = self.storyboard!.instantiateViewController(withIdentifier: "ContactViewController")
                navigationController.setViewControllers([viewController], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
        }
    }
}
