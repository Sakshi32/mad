//
//  MyAvailabilityViewController.swift
//  MAD
//
//  Created by mac on 25/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class MyAvailabilityViewController: UIViewController {
    
    @IBOutlet weak var tblMy: UITableView!
    
    var arrMyAvailability : Array<Any> = []
    var arrSection : Array<Any> = []

    @IBOutlet weak var txtDate: UITextField!
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var txtAddress: UITextView!
    @IBOutlet weak var txtComnment: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.txtDate.delegate = self
        self.txtTime.delegate = self
        self.txtAddress.delegate = self
        self.txtComnment.delegate = self
        
        self.txtAddress.text = "Enter Address"
        self.txtAddress.textColor = UIColor.lightGray
        
        self.txtComnment.text = "Enter Comnment"
        self.txtComnment.textColor = UIColor.lightGray
        
        self.viewBg.isHidden = true
        self.MyAvailability()
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func AddMethod(_ sender: Any) {
        self.viewBg.isHidden = false
    }

    @IBAction func CrossMethod(_ sender: Any) {
        self.viewBg.isHidden = true
    }
    
    @IBAction func SubmitMethod(_ sender: Any) {
        
        guard let name = self.txtDate.text, name != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select date", VC: self)
        }
        
        guard let contact = self.txtTime.text, contact != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select time", VC: self)
        }
        
        guard let address = self.txtAddress.text, address != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter address", VC: self)
        }
        
        guard let comment = self.txtAddress.text, comment != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter comment", VC: self)
        }
        
        self.AddMyAvailability()
    }
    
    
    
    //////// MyAvailability ////////
    func MyAvailability(){
        self.startActivityIndicator()
        let param = ["ACTION"    : "GET"] as [String : Any]
        
        CommunicationManager().getResponseForParamType(strUrl: Url_File.MyAvailability, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.arrMyAvailability = dataDict["slot"] as! Array<Any>
                self.Data(arr: self.arrMyAvailability as! Array<NSDictionary>)
                //self.tblMy.reloadData()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func Data(arr : Array<NSDictionary>)  {
        
      let arrsort = arr.sorted(by: { $0.value(forKey: "book_date") as! String > $1.value(forKey: "book_date") as! String})
        print(arrsort)
        self.arrMyAvailability = arrsort
        
        var first = Common.ChangeDateFormat(Date: arrsort[0].value(forKey: "book_date") as! String, fromFormat: "yyyy-MM-dd", toFormat: "MMM yyyy")
        self.arrSection.append(first)
        
        for item in arrsort{
            let sec = Common.ChangeDateFormat(Date: item.value(forKey: "book_date") as! String, fromFormat: "yyyy-MM-dd", toFormat: "MMM yyyy")
            if first == sec  {
                
            }else{
                self.arrSection.append(sec)
                first = sec
            }
        }
    
        print(arrSection)
        self.tblMy.reloadData()
    }
    
    func getSectionItems(section: Int) -> Array<Any> {
        var sectionItems : Array<Any> = []
        
        // loop through the testArray to get the items for this sections's date
        for item in self.arrMyAvailability {
            let dic = item as! NSDictionary
            let sec = Common.ChangeDateFormat(Date: dic.value(forKey: "book_date") as! String, fromFormat: "yyyy-MM-dd", toFormat: "MMM yyyy")
            if sec == self.arrSection[section] as! String {
                sectionItems.append(dic)
            }
        }
        
        return sectionItems
    }
    
    
    //////// MyAvailability ////////
    func AddMyAvailability(){
        self.startActivityIndicator()
        let param = ["ACTION"    : "ADD",
                     "book_date" : self.txtDate.text!,
                     "start_time" : self.txtTime.text!,
                     "comment" : self.txtComnment.text!,
                     "address" : self.txtAddress.text!,
                     "user_id" : SharedPreference.getUserId()] as [String : Any]
        
        CommunicationManager().getResponseForParamType(strUrl: Url_File.MyAvailability, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.MyAvailability()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }

}

extension MyAvailabilityViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtDate{
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            datePickerView.minimumDate = datePickerView.date
            self.txtDate.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }
        if textField == self.txtTime{
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .time
            //datePickerView.minimumDate = datePickerView.date
            self.txtTime.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleTimePicker(sender:)), for: .valueChanged)
        }
    }
    
    @objc func handleTimePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        self.txtTime.text = dateFormatter.string(from: sender.date)
        //self.strTime = Common.ChangeDateFormat(Date: self.txtTime.text!, fromFormat: "HH:mm", toFormat: "HH:mm:ss")
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.txtDate.text = dateFormatter.string(from: sender.date)
       // self.strDate = Common.ChangeDateFormat(Date: self.txtDate.text!, fromFormat: "dd MMM yyyy", toFormat: "yyyy-MM-dd")
    }
}

extension MyAvailabilityViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrSection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getSectionItems(section: section).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tblMy.dequeueReusableCell(withIdentifier: "MyCell") as! MyAvailabilityCell
        
        //let dic  = self.arrMyAvailability[indexPath.row] as! NSDictionary
        
        //let section = self.arrSection[indexPath.section]
        let sectionItems = self.getSectionItems(section: indexPath.section)
        let dic = sectionItems[indexPath.row] as! NSDictionary
        cell.lblDateTime.text = String("\(dic.value(forKey: "book_date") as! String), \(dic.value(forKey: "start_time") as! String)")
        cell.lblAddress.text = dic.value(forKey: "address") as? String
        cell.lblComment.text = dic.value(forKey: "comment") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
         return self.arrSection[section] as? String
    }
}

class MyAvailabilityCell: UITableViewCell {
    
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
}


extension MyAvailabilityViewController : UITextViewDelegate{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if self.txtAddress.textColor == UIColor.lightGray {
            self.txtAddress.text = ""
            self.txtAddress.textColor = UIColor.black
        }
        if self.txtComnment.textColor == UIColor.lightGray {
            self.txtComnment.text = ""
            self.txtComnment.textColor = UIColor.black
        }
        return true
    }
}



//slot =     (
//    {
//        address = onam;
//        "book_date" = "2018-10-27";
//        comment = Testios;
//        "end_time" = "2018-10-25 00:38:23";
//        request = 0;
//        "slot_id" = 4;
//        "start_time" = "15:07:52";
//},
//    {
//        address = indore;
//        "book_date" = "2018-07-12";
//        comment = no;
//        "end_time" = "2018-10-24 06:57:25";
//        request = 0;
//        "slot_id" = 3;
//        "start_time" = "08:00:00";
//},
//    {
//        address = "<null>";
//        "book_date" = "2018-10-23";
//        comment = "<null>";
//        "end_time" = "2018-10-21 23:10:00";
//        request = 0;
//        "slot_id" = 2;
//        "start_time" = "04:00:00";
//},
//    {
//        address = "";
//        "book_date" = "2018-10-22";
//        comment = "";
//        "end_time" = "2018-10-21 23:10:06";
//        request = 0;
//        "slot_id" = 1;
//        "start_time" = "05:00:00";
//}
//);

