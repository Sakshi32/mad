//
//  HomeViewController.swift
//  MAD
//
//  Created by mac on 12/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMobileAds

class HomeViewController: UIViewController {
    
    @IBOutlet weak var collHome: UICollectionView!
    @IBOutlet weak var bannerHight: NSLayoutConstraint!
    
   
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    var offSet: CGFloat = 0
   // scrollview
   // pageControl
    var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
    
    var interstitial: GADInterstitial!
    var arrImages : [String] = []
    var arrItemName : Array<Any> = []
    var arrItemImages : Array<Any> = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collHome.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Home")
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/1033173712")
        let request = GADRequest()
        interstitial.load(request)
        self.ImageSliderService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        }
        
        sideMenuController?.isLeftViewDisabled = false
        if SharedPreference.getIsUserLogin() == true{
            if SharedPreference.getUserType() == "Admin"{
                self.bannerHight.constant = 0
                self.arrItemName = ["CHANGE BANNER","SPECIAL OFFERS", "GALLERY","BOOKINGS", "ABOUT US", "LOGOUT"]
                self.arrItemImages = ["changebanner", "offer", "gallery", "about", "about", "logout"]
            }else{
                let activity = storyboard?.instantiateViewController(withIdentifier: "UserHomeViewController") as! UserHomeViewController
                self.navigationController?.pushViewController(activity, animated: true)
            }
        }else{
            self.bannerHight.constant = 180
            self.arrItemName = ["GALLERY", "SPECIAL OFFERS", "ABOUT US", "CONTACT US"]
            self.arrItemImages = ["gallery", "offer", "about", "contact"]
        }
        self.collHome.reloadData()
    }
    
    @IBAction func MenuMethod(_ sender: Any) {
        let mainViewController = UIApplication.shared.delegate!.window!!.rootViewController! as! MainViewController
        mainViewController.showLeftView(animated: true, completionHandler: nil)
    }
    

    override func viewDidAppear(_ animated: Bool) {
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
    }
    
    @objc func autoScroll (){
        let totalPossibleOffset = CGFloat(self.arrImages.count - 1) * self.view.bounds.size.width
        if offSet == totalPossibleOffset {
            offSet = 0 // come back to the first image after the last image
        }
        else {
            offSet += self.view.bounds.size.width
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.scrollview.contentOffset.x = CGFloat(self.offSet)
                //self.pageControl.currentPage = self.arrImages.count
            }, completion: nil)
        }
    }
    
    func SetupPages(){
        let screenSize = UIScreen.main.bounds
        let width = screenSize.width
        let hight = screenSize.height - 157
        for index in 0..<self.arrImages.count{
            let strImg = self.arrImages[index]
            let imageView = UIImageView()
            let xPosition = UIScreen.main.bounds.width * CGFloat(index)
            imageView.frame = CGRect(x: xPosition, y: 0, width: scrollview.frame.width, height: 175)
            imageView.sd_setImage(with: URL(string: strImg), placeholderImage: UIImage(named: ""))
            imageView.contentMode = .scaleToFill
            
            self.scrollview.contentSize = CGSize(width: width * CGFloat(index + 1),height: hight -  hight)
            self.scrollview.addSubview(imageView)
        }
        
        pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
    }
    
    func configurePageControl() {
        self.pageControl.numberOfPages = self.arrImages.count
        self.pageControl.currentPage = 0
        self.pageControl.currentPageIndicatorTintColor = UIColor.blue
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    @objc func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * scrollview.frame.size.width
        scrollview.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
   
    func ImageSliderService(){
        self.startActivityIndicator()
        let param = ["SLIDER": "TRUE"] as [String : Any]

        CommunicationManager().getResponseForParamType(strUrl: Url_File.ImageSlider, parameters: param as NSDictionary) { ( result , data) in

            if(result == "success") {
                
                self.stopActivityIndicator()
                let dataDict = data as! NSDictionary
                print(dataDict)
                
                let arrImages = dataDict["image"] as! [NSDictionary]
                for i in 0..<arrImages.count{
                    let dict = arrImages[i]
                    let strImg = dict["img"] as! String
                    self.arrImages.append(strImg)
                }
                self.SetupPages()
                
                self.configurePageControl()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }

}

extension HomeViewController : UICollectionViewDataSource,UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrItemName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Home",
                                                      for: indexPath) as! HomeCollectionViewCell
       
        Cell.lblName.text = self.arrItemName[indexPath.row] as? String
        Cell.imgitem.image = UIImage(named: (self.arrItemImages[indexPath.row] as? String)!)  
        
        return Cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if SharedPreference.getIsUserLogin(){
            if indexPath.row == 0{
                let activity = storyboard?.instantiateViewController(withIdentifier: "ChangeBanneViewController") as! ChangeBanneViewController
                self.navigationController?.pushViewController(activity, animated: true)
            }else if indexPath.row == 1{
                let activity = storyboard?.instantiateViewController(withIdentifier: "OfferViewController") as! OfferViewController
                self.navigationController?.pushViewController(activity, animated: true)
            }
            else if indexPath.row == 2{
                let activity = storyboard?.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
                self.navigationController?.pushViewController(activity, animated: true)
            }
            else if indexPath.row == 3{
                let activity = storyboard?.instantiateViewController(withIdentifier: "BookingViewController") as! BookingViewController
                self.navigationController?.pushViewController(activity, animated: true)
            }
            else if indexPath.row == 4{
                let activity = storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                self.navigationController?.pushViewController(activity, animated: true)
            }
            else if indexPath.row == 5{
                SharedPreference.setIsUserLogin(false)                
                SharedPreference.setUserType("")
                NotificationCenter.default.post(name: Notification.Name("Reload"), object: nil)
                self.viewWillAppear(true)
            }
        }else{
            if indexPath.row == 0{
                let activity = storyboard?.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
                self.navigationController?.pushViewController(activity, animated: true)
            }else if indexPath.row == 1{
                let activity = storyboard?.instantiateViewController(withIdentifier: "OfferViewController") as! OfferViewController
                self.navigationController?.pushViewController(activity, animated: true)
            }
            else if indexPath.row == 2{
                let activity = storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                self.navigationController?.pushViewController(activity, animated: true)
            }
            else if indexPath.row == 3{
                let activity = storyboard?.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
                self.navigationController?.pushViewController(activity, animated: true)
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
    }
}

extension HomeViewController : UICollectionViewDelegateFlowLayout
{
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let padding: CGFloat =  2
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
}
