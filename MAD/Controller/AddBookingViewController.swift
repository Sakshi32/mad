//
//  AddBookingViewController.swift
//  MAD
//
//  Created by mac on 16/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class AddBookingViewController: UIViewController {

    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    
    var strDate : String = ""
    var strTime : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtDate.delegate = self
        self.txtTime.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sideMenuController?.isLeftViewDisabled = true
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func AddMethod(_ sender: Any) {
        guard let name = self.txtDate.text, name != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select date", VC: self)
        }
        
        guard let contact = self.txtTime.text, contact != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select time", VC: self)
        }
        
        self.AddBooking()
    }
    
    func AddBooking(){
        self.startActivityIndicator()
        let param = ["ACTION"    : "ADD",
                     "booking_date" : self.strDate,
                     "booking_time" : self.strTime,
                     "duration" : "1",] as [String : Any]
        
        CommunicationManager().getResponseForParamType(strUrl: Url_File.AddBooking, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
               self.ShowPopUp()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func ShowPopUp()  {
        let alert = UIAlertController(title: Common.Title, message:
            "Successfully add time slot", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.default) { (action) in
            Common.PopMethod(VC: self)
        }
        
       // let CancleAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (action) in
            
       // }
        
        alert.addAction(okAction)
        //alert.addAction(CancleAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension AddBookingViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtDate{
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            datePickerView.minimumDate = datePickerView.date
            self.txtDate.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }
        if textField == self.txtTime{
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .time
           //datePickerView.minimumDate = datePickerView.date
            self.txtTime.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleTimePicker(sender:)), for: .valueChanged)
        }
    }
    
    @objc func handleTimePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        self.txtTime.text = dateFormatter.string(from: sender.date)
        self.strTime = Common.ChangeDateFormat(Date: self.txtTime.text!, fromFormat: "HH:mm", toFormat: "HH:mm:ss")
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        self.txtDate.text = dateFormatter.string(from: sender.date)
        self.strDate = Common.ChangeDateFormat(Date: self.txtDate.text!, fromFormat: "dd MMM yyyy", toFormat: "yyyy-MM-dd")
    }
}
