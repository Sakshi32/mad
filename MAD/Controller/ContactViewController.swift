//
//  ContactViewController.swift
//  MAD
//
//  Created by mac on 13/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {

    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtContactNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMessage: UITextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sideMenuController?.hideLeftView()
        sideMenuController?.isLeftViewDisabled = true
    }

    @IBAction func SubmitMethod(_ sender: Any) {
        
        guard let name = self.txtName.text, name != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter Name", VC: self)
        }
        
        guard let contact = self.txtContactNumber.text, contact != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter contact number", VC: self)
        }
        
        guard let email = self.txtEmail.text, email != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter E-mail", VC: self)
        }
        
        guard Common.isValidEmail(testStr: self.txtEmail.text!)  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter valid E-mail", VC: self)
        }
        
        guard let msg = self.txtMessage.text, msg != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter message", VC: self)
        }
        
        self.ContactService()
        
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! NavigationController
        let viewController: UIViewController!
        viewController = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController")
        navigationController.setViewControllers([viewController], animated: false)
        mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
    }
    
    //////////// Contact ////////
    func ContactService(){
        self.startActivityIndicator()
        let param = ["NAME": self.txtName.text!,
                     "EMAIL": self.txtEmail.text!,
                     "CONTACT_NUM": self.txtContactNumber.text!,
                     "MESSAGE": self.txtMessage.text!,
                     "ACTION" : "ADD"] as [String : Any]

        CommunicationManager().getResponseForParamType(strUrl: Url_File.ContactUs, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.stopActivityIndicator()
                Common.ShowAlert(Title: Common.Title, Message: "Message send successfully", VC: self)
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}

extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
}
