//
//  AboutUsViewController.swift
//  MAD
//
//  Created by mac on 12/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    @IBOutlet weak var imgAbout: UIImageView!
    @IBOutlet weak var lblContant: UILabel!
    @IBOutlet weak var txtContant: UITextView!
    @IBOutlet weak var btnDone: UIButton!
    
    var about_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sideMenuController?.hideLeftView()
        sideMenuController?.isLeftViewDisabled = true
        
        if SharedPreference.getIsUserLogin(){
            if SharedPreference.getUserType() == "Admin"{
                self.btnDone.isHidden = false
            }else{
                self.btnDone.isHidden = true
            }
        }else{
            self.btnDone.isHidden = true
        }
        self.AboutService()
    }

    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
        
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! NavigationController
        let viewController: UIViewController!
        viewController = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController")
        navigationController.setViewControllers([viewController], animated: false)
        mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
    }
    
    @IBAction func DoneMethod(_ sender: Any) {
        guard let name = self.txtContant.text, name != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter text", VC: self)
        }
        self.AboutEditService()
    }
    
    //About
    func AboutService() {
        self.startActivityIndicator()
        CommunicationManager().getResponseFor(strUrl: Url_File.About, parameters: nil, completion: { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let arrRes = dataDict["responce"] as! Array<NSDictionary>
                self.ShowData(dic: arrRes[0])
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        })
    }

    func ShowData(dic : NSDictionary){
        if SharedPreference.getIsUserLogin(){
            if SharedPreference.getUserType() == "Admin"{
                self.lblContant.isHidden = true
                self.txtContant.isHidden = false
                self.txtContant.text = dic.value(forKey: "about_text") as? String
            }else{
                self.lblContant.isHidden = false
                self.txtContant.isHidden = true
                self.lblContant.text = dic.value(forKey: "about_text") as? String
            }
        }else{
            self.lblContant.isHidden = false
            self.txtContant.isHidden = true
            self.lblContant.text = dic.value(forKey: "about_text") as? String
        }
        
        self.about_id = dic.value(forKey: "about_id") as! String
        let img = dic.value(forKey: "image") as! String
        self.imgAbout.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "placeholder"))
    }
    
    //////////// About Edit ////////
    func AboutEditService(){
        self.startActivityIndicator()
        let param = ["about_data": self.txtContant.text!,
                     "about_id": self.about_id,
                     "ACTION" : "ADD"] as [String : Any]
    
        CommunicationManager().getResponseForParamType(strUrl: Url_File.AboutEdit, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.stopActivityIndicator()
                Common.ShowAlert(Title: Common.Title, Message: "Message send successfully", VC: self)
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}
